namespace MC.Core
{
    public interface IInjectable<T>
    {
        void Inject(T injectee);
    }
}
