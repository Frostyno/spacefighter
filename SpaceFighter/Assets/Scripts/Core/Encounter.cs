using UnityEngine;

namespace MC.Core
{
    [CreateAssetMenu(fileName = "Encounter", menuName = "SpaceFighter/Core/Encounter")]
    public class Encounter : ScriptableObject
    {
        [System.Serializable]
        public struct EncounterItem
        {
            [SerializeField] private float _spawnTimeSecs;
            [SerializeField] private EncounterObject _prefab;
            [SerializeField] private Vector2 _spawnPosition;
            [SerializeField] private float _spawnRotation;

            public float SpawnTimeSecs => _spawnTimeSecs;
            public EncounterObject Prefab => _prefab;
            public Vector2 SpawnPosition => _spawnPosition;
            public float SpawnRotation => _spawnRotation;
        }

        [Tooltip("Specifies whether the countdown to start this encounter should start with spawning or previous encounter or after previous one has been cleared.")]
        [SerializeField] private bool _startCountdownWithPrevious = false;
        [SerializeField] private float _spawnDelay = 0f;
        [SerializeField] private EncounterItem[] _items = new EncounterItem[0];

        public bool StartCountdownWithPrevious => _startCountdownWithPrevious;
        public float SpawnDelay => _spawnDelay;
        public EncounterItem[] Items => _items;
    }
}
