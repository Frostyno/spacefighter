using System;
using UnityEngine;

namespace MC.Core
{
    public class EncounterObject : ContextMono
    {
        public event Action<EncounterObject> OnEncounterObjectDestroyed = null;

        private void OnDestroy()
        {
            OnEncounterObjectDestroyed?.Invoke(this);
        }
    }
}
