using UnityEngine;

namespace MC.Core
{
    [CreateAssetMenu(fileName = "Level", menuName = "SpaceFighter/Core/Level")]
    public class Level : ScriptableObject
    {
        [SerializeField] private Encounter[] _encounters = new Encounter[0];

        public Encounter[] Encounters => _encounters;
    }
}
