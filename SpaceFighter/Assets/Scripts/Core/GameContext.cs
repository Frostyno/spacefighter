using MC.Models;
using UnityEngine;

namespace MC.Core
{
    public class GameContext
    {
        /* Managers */
        public readonly AppManager AppManager;
        public readonly GameManager GameManager;
        public readonly ScreenManager ScreenManager;
        public readonly SceneLoader SceneLoader;
        public readonly PlayerInputManager PlayerInputManager;
        public readonly EnemyManager EnemyManager;
        public readonly LevelManager LevelManager;
        public readonly SquadronManager SquadronManager;
        public readonly CameraManager CameraManager;
        public readonly SoundManager SoundManager;

        /* Models */
        public readonly PlayerModel PlayerModel;

        /* Misc */
        public readonly Transform ProjectileParent;
        public readonly Transform EncounterObjectsParent;
        public readonly Transform AudioSourcesParent;

        public GameContext()
        {
            AppManager = new AppManager(this);
            GameManager = new GameManager(this);
            ScreenManager = new ScreenManager(this);
            SceneLoader = new SceneLoader(this);
            PlayerInputManager = new PlayerInputManager(this);
            EnemyManager = new EnemyManager(this);
            LevelManager = new LevelManager(this);
            SquadronManager = new SquadronManager(this);
            CameraManager = new CameraManager(this);
            SoundManager = new SoundManager(this);

            PlayerModel = new PlayerModel();

            ProjectileParent = new GameObject("[Projectiles]").transform;
            ProjectileParent.SetParent(null);
            EncounterObjectsParent = new GameObject("[EncounterObjects]").transform;
            EncounterObjectsParent.SetParent(null);
            AudioSourcesParent = new GameObject("[AudioSources]").transform;
            AudioSourcesParent.SetParent(null);
        }
    }
}
