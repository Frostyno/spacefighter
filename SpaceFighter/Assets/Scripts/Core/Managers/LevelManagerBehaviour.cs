namespace MC.Core
{
    public class LevelManagerBehaviour : ContextMono
    {
        [UnityEngine.SerializeField] private Level _level = null;

        private void Start()
        {
            Context.LevelManager.SetBehaviour(this);
            Context.LevelManager.SetLevel(_level, true);
        }

        private void Update()
        {
            Context.LevelManager.Tick();
        }
    }
}
