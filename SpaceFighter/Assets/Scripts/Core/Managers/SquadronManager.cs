using MC.Helpers.Enums;
using MC.Movement;
using System.Collections.Generic;

namespace MC.Core
{
    public class SquadronManager : Manager
    {
        private Dictionary<SquadronType, SquadronPositionsProvider> _squadronPositionsProviders;

        public SquadronManager(GameContext context) : base(context) 
        {
            _squadronPositionsProviders = new Dictionary<SquadronType, SquadronPositionsProvider>()
            {
                { SquadronType.Grid, new GridSquadronPositionsProvider() },
            };
        }

        public void SetWaypointToSquadron(Squadron squadron, Waypoint waypoint)
        {
            if (squadron.Settings == null) return;
            if (!_squadronPositionsProviders.TryGetValue(squadron.Settings.SquadronType, out var provider))
            {
                UnityEngine.Debug.LogError($"SquadronManager is missing ISquadronPositionProvider for SquadronType '{squadron.Settings.SquadronType}'.");
                return;
            }

            provider.SetWaypointToSquadron(squadron, waypoint);
        }
    }
}
