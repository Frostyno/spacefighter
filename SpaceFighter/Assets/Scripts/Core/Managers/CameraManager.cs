using UnityEngine;

namespace MC.Core
{
    public class CameraManager : Manager
    {
        private CameraManagerBehaviour _behaviour = null;

        private Coroutine _stopCoroutine = null;

        public CameraManager(GameContext context) : base(context) { }

        public void SetBehaviour(CameraManagerBehaviour behaviour) =>
            _behaviour = behaviour;

        public bool IsSeenByActiveCamera(Collider2D collider) =>
            _behaviour.IsInViewOfCamera(collider);

        public void ShakeCamera(float shakeStrength, float shakeDurationSecs)
        {
            var currentShakeStrength = _behaviour.CurrentAmplitude;
            bool isCurrentShakeStronger = (currentShakeStrength.HasValue && currentShakeStrength.Value > shakeStrength);
            if (isCurrentShakeStronger)
                return;

            _behaviour.ShakeCamera(shakeStrength);

            if (_stopCoroutine != null)
                _behaviour.StopCoroutine(_stopCoroutine);

            _stopCoroutine = _behaviour.StartCoroutine(WaitAndEndShake(shakeDurationSecs));
        }

        private System.Collections.IEnumerator WaitAndEndShake(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            _behaviour.StopCameraShake();
            _stopCoroutine = null;
        }
    }
}
