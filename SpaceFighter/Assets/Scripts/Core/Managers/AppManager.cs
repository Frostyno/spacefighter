namespace MC.Core
{
    public class AppManager : Manager
    {
        private AppManagerBehaviour _behaviour;

        public AppManager(GameContext context) : base(context) { }

        public void SetBehaviour(AppManagerBehaviour behaviour)
            => _behaviour = behaviour;
    }
}
