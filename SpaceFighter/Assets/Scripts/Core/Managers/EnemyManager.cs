using System.Collections.Generic;
using System;
using MC.Entities;
using MC.Models;

namespace MC.Core
{
    public class EnemyManager : Manager
    {
        private long _enemyIndexer = 0;
        private Dictionary<long, EnemyController> _enemies = new Dictionary<long, EnemyController>();

        public event Action<EnemyModel> OnEnemyDestroyed = null; 

        public EnemyManager(GameContext context) : base(context) { }

        public void RegisterEnemy(EnemyController enemy)
        {
            bool enemyRegistered = (enemy.Model is EnemyModel enemyModel) && enemyModel.ID.HasValue;
            if (enemyRegistered) return;

            enemyModel = enemy.Initialize(_enemyIndexer++);
            _enemies.Add(enemyModel.ID.Value, enemy);

            enemyModel.OnDestroyed.Event += HandleEnemyDestroyed;
        }

        private void HandleEnemyDestroyed(EntityModel entityModel)
        {
            var enemyModel = entityModel as EnemyModel;
            if (enemyModel == null) return;

            if (enemyModel.ID.HasValue)
                _enemies.Remove(enemyModel.ID.Value);
            else
                UnityEngine.Debug.LogWarning($"Received OnHealthDepleted event from unregistered enemy. Disposing anyway..");

            OnEnemyDestroyed?.Invoke(enemyModel);
            UnityEngine.GameObject.Destroy(enemyModel.Entity.gameObject);
        }

    }
}
