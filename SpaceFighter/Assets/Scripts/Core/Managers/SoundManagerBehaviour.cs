using System;
using MC.Helpers.Enums;
using UnityEngine;

namespace MC.Core
{
    public class SoundManagerBehaviour : ContextMono
    {
        [SerializeField] private SoundManager.SoundData[] _sounds = new SoundManager.SoundData[0];

        private int _backgroundMusicHandle = -1;
        
        public SoundManager.SoundData[] Sounds => _sounds;

        private void Start()
        {
            Context.SoundManager.SetBehaviour(this);
            _backgroundMusicHandle = Context.SoundManager.PlayContinuousSound(Sound.Background);
        }
    }
}
