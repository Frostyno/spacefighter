using Cinemachine;
using UnityEngine;

namespace MC.Core
{
    public class CameraManagerBehaviour : ContextMono
    {
        [SerializeField] private CinemachineBrain _cinemachineBrain = null;

        private CinemachineVirtualCamera _activeCamera = null;
        private CinemachineBasicMultiChannelPerlin _noise = null;
        private float _defaultFrequency;

        public float? CurrentAmplitude => _activeCamera != null ? _noise?.m_AmplitudeGain : null;

        private void Awake()
        {
            if (_cinemachineBrain != null) return;

            _cinemachineBrain = FindObjectOfType<CinemachineBrain>(true);
            if (_cinemachineBrain == null)
                Debug.LogError("CameraManagerBehaviour requires CinemachineBrain to function properly.");
        }

        private void Start()
        {
            Context.CameraManager.SetBehaviour(this);
        }

        public bool IsInViewOfCamera(Collider2D collider)
        {
            if (collider == null)
                return false;
            if (_cinemachineBrain.OutputCamera == null)
                return false;

            var planes = GeometryUtility.CalculateFrustumPlanes(_cinemachineBrain.OutputCamera);
            return GeometryUtility.TestPlanesAABB(planes, collider.bounds);
        }

        public void ShakeCamera(float amplitude, float? frequency = null)
        {
            CheckActiveCamera();
            if (_activeCamera == null) return;

            _noise.m_FrequencyGain = frequency ?? _defaultFrequency;
            _noise.m_AmplitudeGain = amplitude;
        }

        public void StopCameraShake()
        {
            if (_activeCamera == null) return;

            _noise.m_AmplitudeGain = 0f;
            _noise.m_FrequencyGain = _defaultFrequency;
        }

        private void CheckActiveCamera()
        {
            if ((object)_activeCamera == _cinemachineBrain.ActiveVirtualCamera)
                return;

            SetActiveCamera(_cinemachineBrain.ActiveVirtualCamera as CinemachineVirtualCamera);
        }

        private void SetActiveCamera(CinemachineVirtualCamera camera)
        {
            if (camera == null) return;
            if (_activeCamera != null)
                StopCameraShake();

            _activeCamera = camera;
            _noise = _activeCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            if (_noise == null)
            {
                Debug.LogError("Active camera is missing CinemachineBasicMultiChannelPerlin.");
                return;
            }

            _defaultFrequency = _noise.m_FrequencyGain;
        }
    }
}
