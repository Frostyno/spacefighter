namespace MC.Core
{
    public class InputManagerBehvaiour : ContextMono
    {
        private PlayerInputManager _inputManager;

        private void Start()
        {
            _inputManager = Context.PlayerInputManager;
            _inputManager.SetBehaviour(this);
        }

        private void Update()
        {
            _inputManager.Tick();
        }
    }
}
