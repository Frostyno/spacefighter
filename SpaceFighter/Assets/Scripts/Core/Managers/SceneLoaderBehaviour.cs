using System.Collections.Generic;
using IEnumerator = System.Collections.IEnumerator;
using ICommand = MC.Commands.ICommand;
using Scene = MC.Helpers.Enums.Scene;
using UnityEngine.SceneManagement;

namespace MC.Core
{
    public class SceneLoaderBehaviour : ContextMono
    {
        private void Start()
        {
            Context.SceneLoader.SetBehaviour(this);
        }

        public IEnumerator LoadScene(Scene scene, List<ICommand> afterLoadCommands = null)
        {
            var s = SceneManager.GetSceneByBuildIndex((int)scene);
            if (s.isLoaded) // MH: don't allow duplicit scene loading
                yield break;

            var asyncOp = SceneManager.LoadSceneAsync((int)scene, LoadSceneMode.Additive);
            while (!asyncOp.isDone)
            {
                // TODO: add loading screen
                yield return null;
            }


            afterLoadCommands?.ForEach(c => c.Execute());
        }

        public IEnumerator UnloadScene(Scene scene, List<ICommand> afterUnloadCommands = null)
        {
            var s = SceneManager.GetSceneByBuildIndex((int)scene);
            if (!s.isLoaded)
                yield break;

            var asyncOp = SceneManager.UnloadSceneAsync((int)scene);
            while (!asyncOp.isDone)
            {
                // MH: maybe not needed here and can just yield return asyncOp
                yield return null;
            }

            afterUnloadCommands?.ForEach(c => c.Execute());
        }
    }
}
