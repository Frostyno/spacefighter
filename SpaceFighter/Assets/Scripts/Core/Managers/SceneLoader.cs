using System.Collections.Generic;
using IEnumerator = System.Collections.IEnumerator;
using ICommand = MC.Commands.ICommand;
using Scene = MC.Helpers.Enums.Scene;

namespace MC.Core
{
    public class SceneLoader : Manager
    {
        private SceneLoaderBehaviour _behaviour;

        public SceneLoader(GameContext context) : base(context) { }

        public void SetBehaviour(SceneLoaderBehaviour behaviour) =>
            _behaviour = behaviour;

        public IEnumerator LoadScene(Scene scene, List<ICommand> afterLoadCommands = null)
        {
            yield return _behaviour.LoadScene(scene, afterLoadCommands);
        }

        public IEnumerator UnloadScene(Scene scene, List<ICommand> afterUnloadCommands = null)
        {
            yield return _behaviour.UnloadScene(scene, afterUnloadCommands);
        }
    }
}
