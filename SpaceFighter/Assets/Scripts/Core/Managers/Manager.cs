namespace MC.Core
{
    public abstract class Manager
    {
        protected GameContext Context { get; private set; }

        public Manager(GameContext context)
        {
            Context = context;
        }
    }
}
