using IEnumerator = System.Collections.IEnumerator;

namespace MC.Core
{
    public class AppManagerBehaviour : ContextMono
    {
        private void Awake()
        {
            SetContext(new GameContext());
        }

        private IEnumerator Start()
        {
            Context.AppManager.SetBehaviour(this);

            yield return null; // MH: workaround to wait for all managers to load behaviours, resolve in a different wayu
            yield return Context.SceneLoader.LoadScene(Helpers.Enums.Scene.TestScene);
        }
    }
}
