using System;
using System.Collections;
using System.Collections.Generic;
using MC.Helpers.Enums;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MC.Core
{
    public class SoundManager : Manager
    {
        [Serializable]
        public class SoundData
        {
            public Sound Sound = Sound.None;
            public AudioClip Clip = null;
            public float Volume = 1;
            public float Pitch = 1;
        }
        
        private int _audioSourceIndexer = 0;
        private int _audioSourceHandleIndexer = 0;
        
        private SoundManagerBehaviour _behaviour = null;
        private Dictionary<Sound, List<SoundData>> _sounds = new Dictionary<Sound, List<SoundData>>();
        private Queue<AudioSource> _audioSources = new Queue<AudioSource>();
        private Dictionary<int, AudioSource> _audioSourcesInUse = new Dictionary<int, AudioSource>();
        
        public SoundManager(GameContext context) : base(context) {}

        public void SetBehaviour(SoundManagerBehaviour behaviour)
        {
            _behaviour = behaviour;
            LoadSounds();
        }

        private void LoadSounds()
        {
            if (!_behaviour) return;

            foreach (var soundData in _behaviour.Sounds)
            {
                if (soundData.Sound == Sound.None)
                {
                    Debug.LogWarning("Sound has it's enum value set to None!");
                    continue;
                }
                
                if (!soundData.Clip) continue;

                if (!_sounds.TryGetValue(soundData.Sound, out var sounds))
                {
                    sounds = new List<SoundData>();
                    _sounds.Add(soundData.Sound, sounds);
                }
                
                sounds.Add(soundData);
            }
        }

        public int PlayContinuousSound(Sound sound) // TODO: how to allow for position updating?
        {
            var soundData = SelectRandomSound(sound);
            if (soundData == null)
                return -1;
            
            var audioSource = GetAudioSource();
            audioSource.loop = true;
            SetSoundDataToSource(audioSource, soundData);

            var handle = _audioSourceHandleIndexer++;
            _audioSourcesInUse.Add(handle, audioSource);
            audioSource.Play();
            return handle;
        }

        public void ReleaseAudioSourceByHandle(int handle)
        {
            if (!_audioSourcesInUse.TryGetValue(handle, out var audioSource))
                return;

            _audioSourcesInUse.Remove(handle);
            _audioSources.Enqueue(audioSource);
        }

        public void PlaySound(Vector2 sourcePosition, Sound sound)
        {
            var soundData = SelectRandomSound(sound);
            if (soundData == null)
                return;
            
            var audioSource = GetAudioSource();
            audioSource.loop = false;
            audioSource.transform.position = sourcePosition;
            SetSoundDataToSource(audioSource, soundData);

            audioSource.Play();
            _behaviour.StartCoroutine(ReleaseAudioSource(audioSource, audioSource.clip.length));
        }

        private SoundData SelectRandomSound(Sound sound)
        {
            if (_sounds.TryGetValue(sound, out var sounds)) 
                return sounds[Random.Range(0, sounds.Count)];
            
            Debug.LogWarning($"No clip available for sound {sound}!");
            return null;
        }

        private void SetSoundDataToSource(AudioSource source, SoundData data)
        {
            if (!source || data == null) return;

            source.clip = data.Clip;
            source.volume = data.Volume;
            source.pitch = data.Pitch;
        }

        private IEnumerator ReleaseAudioSource(AudioSource audioSource, float delay)
        {
            yield return new WaitForSeconds(delay);
            _audioSources.Enqueue(audioSource);
        }

        private AudioSource GetAudioSource()
        {
            if (_audioSources.Count > 0)
                return _audioSources.Dequeue();

            var audioSource = new GameObject($"AudioSource #{_audioSourceIndexer++}").AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            return audioSource;
        }
    }
}
