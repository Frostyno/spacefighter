using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using IEnumerator = System.Collections.IEnumerator;
using System;

namespace MC.Core
{
    public class LevelManager : Manager
    {
        private LevelManagerBehaviour _behaviour = null;

        private Level _activeLevel = null;
        private bool _levelStarted = false;
        private int _currentEncounterIndex = -1;
        private bool _previousEncounterSpawned = true;
        private float? _encounterSpawnTimer = .0f;

        private List<EncounterObject> _encounterObjects = new List<EncounterObject>();

        public LevelManager(GameContext context) : base(context) { }

        public void SetBehaviour(LevelManagerBehaviour behaviour) =>
            _behaviour = behaviour;

        public void SetLevel(Level level, bool start = false)
        {
            if (level == null) return;
            _levelStarted = false;
            _activeLevel = level;

            if (start)
                StartLevel();
        }

        public void StartLevel()
        {
            if (_activeLevel == null) return;
            if (_levelStarted) return;

            _levelStarted = true;
            _currentEncounterIndex = -1;
            _previousEncounterSpawned = true;
            _encounterSpawnTimer = null;
            _encounterObjects.Clear();
        }

        public void Tick()
        {
            if (_activeLevel == null) return;
            if (!_levelStarted) return;

            UpdateTimers();
            CheckSpawnNextEncounter();
        }

        private void CheckSpawnNextEncounter()
        {
            if (!_previousEncounterSpawned) return;
            if (_activeLevel.Encounters.Length <= (_currentEncounterIndex + 1))
                return;

            var nextEncounter = _activeLevel.Encounters[_currentEncounterIndex + 1];
            bool shouldInitializeTimer = !_encounterSpawnTimer.HasValue && (_encounterObjects.Count == 0);
            if (shouldInitializeTimer)
                _encounterSpawnTimer = .0f;

            bool shouldSpawnEncounter = _encounterSpawnTimer.HasValue && _encounterSpawnTimer.Value >= nextEncounter.SpawnDelay;
            if (!shouldSpawnEncounter) return;

            ++_currentEncounterIndex;
            _behaviour.StartCoroutine(SpawnEncounter());
        }

        private IEnumerator SpawnEncounter()
        {
            if (_activeLevel == null) yield break;
            if (_currentEncounterIndex >= _activeLevel.Encounters.Length)
                yield break;

            _previousEncounterSpawned = false;
            float timeSinceEncounterStart = 0f;
            List<Encounter.EncounterItem> _itemsToSpawn = _activeLevel.Encounters[_currentEncounterIndex].Items.Where(i => i.Prefab != null).ToList();
            List<Encounter.EncounterItem> _spawnedThisFrame = new List<Encounter.EncounterItem>();

            while (_itemsToSpawn.Count != 0)
            {
                foreach (var item in _itemsToSpawn)
                {
                    if (item.SpawnTimeSecs > timeSinceEncounterStart) continue;

                    var encounterObject = GameObject.Instantiate(item.Prefab, Context.EncounterObjectsParent);
                    encounterObject.transform.position = item.SpawnPosition;
                    encounterObject.transform.eulerAngles = encounterObject.transform.eulerAngles.With(z: item.SpawnRotation);

                    encounterObject.OnEncounterObjectDestroyed += HandleEncounterObjectDestroyed;
                    _encounterObjects.Add(encounterObject);
                    _spawnedThisFrame.Add(item);
                }

                if (_spawnedThisFrame.Count > 0)
                {
                    _itemsToSpawn = _itemsToSpawn.Except(_spawnedThisFrame).ToList();
                    _spawnedThisFrame.Clear();
                }

                yield return null;
                timeSinceEncounterStart += Time.deltaTime;
            }

            _previousEncounterSpawned = true;

            bool startTimer = ((_currentEncounterIndex + 1) < _activeLevel.Encounters.Length) 
                            && _activeLevel.Encounters[_currentEncounterIndex + 1].StartCountdownWithPrevious;

            _encounterSpawnTimer = startTimer ? (float?).0f : null;
        }

        private void UpdateTimers()
        {
            if (_encounterSpawnTimer.HasValue)
                _encounterSpawnTimer += Time.deltaTime;
        }

        private void HandleEncounterObjectDestroyed(EncounterObject encounterObject) =>
            _encounterObjects.Remove(encounterObject);
    }
}
