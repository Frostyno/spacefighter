using MC.InputSystem;
using System.Collections.Generic;

namespace MC.Core
{
    public class PlayerInputManager : Manager
    {
        private InputManagerBehvaiour _behaviour;
        private List<IPlayerInputController> _inputControllers; // MH: maybe later use Dictionary and separate by input type

        public PlayerInputManager(GameContext context) : base(context)
        {
            _inputControllers = new List<IPlayerInputController>();
        }

        public void SetBehaviour(InputManagerBehvaiour behaviour) =>
            _behaviour = behaviour;

        public void Tick()
        {
            _inputControllers.ForEach(ic => ic.Tick());
        }

        public IPlayerInputController RequestInputController(InputType inputType)
        {
            IPlayerInputController inputController = InputControllerFactory.Create(inputType);
            _inputControllers.Add(inputController);

            return inputController;
        }

        public void DisposeInputController(IPlayerInputController inputController) =>
            _inputControllers.Remove(inputController);
    }
}
