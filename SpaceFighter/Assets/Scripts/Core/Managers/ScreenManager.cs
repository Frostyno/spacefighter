using MC.UI;
using Type = System.Type;
using System.Collections.Generic;

namespace MC.Core
{
    public class ScreenManager : Manager
    {
        private Dictionary<Type, ScreenBase> _screens = new Dictionary<Type, ScreenBase>();

        public ScreenManager(GameContext context) : base(context) { }

        public void RegisterCanvas(CanvasRoot root)
        {
            if (root == null)
                return;

            List<ScreenBase> screens = root.GetScreens();
            screens.ForEach(s => _screens[s.GetType()] = s);
        }

        public void UnregisterCanvas(CanvasRoot root)
        {
            if (root == null)
                return;

            List<ScreenBase> screens = root.GetScreens();
            screens.ForEach(s => _screens.Remove(s.GetType()));
        }

        public void ShowScreen<T>(bool hideOthers = false)
        {
            ScreenBase screen = GetScreen<T>();
            if (screen == null)
            {
                UnityEngine.Debug.LogError($"No screen for type '{typeof(T)}' found.");
                return;
            }

            if (hideOthers)
                foreach (KeyValuePair<Type, ScreenBase> screenPair in _screens)
                    screenPair.Value.Hide();

            screen.Show();
        }

        public void HideScreen<T>() =>
            GetScreen<T>().Hide();

        private ScreenBase GetScreen<T>()
        {
            if (_screens.TryGetValue(typeof(T), out ScreenBase screen))
                return screen;

            return null;
        }
    }
}
