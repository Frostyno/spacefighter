using UnityEngine;

namespace MC.Core
{
    public abstract class ContextMono : MonoBehaviour
    {
        private static GameContext _gameContext;
        public GameContext Context => _gameContext;

        protected void SetContext(GameContext context)
        {
            if (_gameContext != null)
                return;

            _gameContext = context;
        }
    }
}
