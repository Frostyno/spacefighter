using MC.Core;
using System.Collections.Generic;
using MC.Models;
using SerializeField = UnityEngine.SerializeField;
using WeaponGroup = MC.Helpers.Enums.WeaponGroup;
using WeaponDefinition = MC.Definitions.WeaponDefinition;

namespace MC.Weapons
{
    public class WeaponSpawnPoint : ContextMono, IInjectable<EntityModel>
    {
        [SerializeField] private WeaponGroup _weaponGroup = WeaponGroup.Primary;

        private EntityModel _entityModel = null;
        private Weapon _weapon = null;

        public WeaponGroup WeaponGroup => _weaponGroup;

        public void Inject(EntityModel entityModel) =>
            _entityModel = entityModel;

        public void SpawnWeapon(WeaponDefinition weaponDefinition, UnityEngine.LayerMask validTargetLayers)
        {
            if (_entityModel == null)                           return;
            if (weaponDefinition == null)                       return;
            if (weaponDefinition.WeaponPrefab == null)          return;
            if (weaponDefinition.WeaponGroup != _weaponGroup)   return;

            List<Weapon> groupWeapons;
            if (_weapon != null)
            {
                // Remove existing weapon from model
                if (_entityModel.Weapons.TryGetValue(_weaponGroup, out groupWeapons))
                    groupWeapons.Remove(_weapon);

                Destroy(_weapon.gameObject);                            // TODO: later this should play some animation
            }

            // Create and store new weapon in model
            _weapon = Instantiate(weaponDefinition.WeaponPrefab, transform);
            var weaponModel = new WeaponModel(weaponDefinition, _weapon, validTargetLayers);
            _weapon.SetModel(weaponModel);

            if (!_entityModel.Weapons.TryGetValue(_weaponGroup, out groupWeapons))
            {
                groupWeapons = new System.Collections.Generic.List<Weapon>();
                _entityModel.Weapons.Add(_weaponGroup, groupWeapons);
            }

            groupWeapons.Add(_weapon);
        }
    }
}
