using MC.Core;
using System.Linq;
using WeaponDefinition = MC.Definitions.WeaponDefinition;
using EntityModel = MC.Models.EntityModel;
using WeaponGroup = MC.Helpers.Enums.WeaponGroup;
using UnityEngine;

namespace MC.Weapons
{
    public class WeaponsController : ContextMono, IInjectable<EntityModel>
    {
        private EntityModel _entityModel = null;

        public void Inject(EntityModel entityModel) =>
            _entityModel = entityModel;

        public void SpawnWeapons(WeaponDefinition[] weaponDefinitions, LayerMask validTargetLayers)
        {
            var weaponSpawnPoints = GetComponentsInChildren<WeaponSpawnPoint>(true).ToList();
            foreach (var definition in weaponDefinitions)
            {
                WeaponSpawnPoint spawnPoint = weaponSpawnPoints.FirstOrDefault(sp => sp.WeaponGroup == definition.WeaponGroup);
                if (spawnPoint == null)
                {
                    UnityEngine.Debug.LogError($"Not enough spawn points for Entity {name} in WeaponGroup {definition.WeaponGroup}");
                    continue;
                }

                weaponSpawnPoints.Remove(spawnPoint);
                spawnPoint.SpawnWeapon(definition, validTargetLayers);
            }
        }

        public void SetWeaponGroupActive(WeaponGroup group, bool active)
        {
            if (!_entityModel.Weapons.TryGetValue(group, out var groupWeapons))
                return;

            groupWeapons.ForEach(w => w.SetActive(active));
        }
    }
}
