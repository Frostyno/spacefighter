using UnityEngine;
using System.Collections.Generic;
using WeaponModel = MC.Models.WeaponModel;
using GameContext = MC.Core.GameContext;
using SpawnPointProjectilePair = System.Tuple<UnityEngine.Transform, UnityEngine.GameObject>;

namespace MC.Weapons
{
    /// <summary>
    /// Fire provider that retains control over spawned projectiles.
    /// Each frame position and rotation of projectiles is set to the 
    /// position and of assigned spawn point. Used for lasers, flamethrowers
    /// and similar.
    /// </summary>
    public class ContinuousFireProvider : FireProvider
    {
        private List<SpawnPointProjectilePair> _spawnPointProjectiles;

        public ContinuousFireProvider(WeaponModel model, GameContext context) : base(model, context)
        {
            _spawnPointProjectiles = new List<SpawnPointProjectilePair>(model.ProjectileSpawnPoints.Length);
            if (Model.ProjectilePrefab == null) return;

            foreach (var spawnPoint in Model.ProjectileSpawnPoints)
            {
                var projectileGO = GameObject.Instantiate(Model.ProjectilePrefab, Context.ProjectileParent);
                var projectile = projectileGO.GetComponentInChildren<Projectile>(true);
                if (projectile != null)
                    projectile.FiredWeaponModel = Model;

                projectileGO.SetActive(false);
                _spawnPointProjectiles.Add(System.Tuple.Create(spawnPoint, projectileGO));
            }
        }

        public override void SetActive(bool active)
        {
            base.SetActive(active);

            if (!Depleted)
                SetProjectilesActive(active);
        }

        public override void Tick()
        {
            Fire();
        }

        private void Fire()
        {
            if (!Active) return;
            if (Depleted) return;

            float availableAmmo = UseAmmo(Time.deltaTime);
            if (availableAmmo <= 0f)
            {
                SetProjectilesActive(false);
                return;
            }

            foreach (var pair in _spawnPointProjectiles)
            {
                pair.Item2.transform.position = pair.Item1.position;
                pair.Item2.transform.rotation = pair.Item1.rotation;
            }
        }

        private void SetProjectilesActive(bool active) =>
            _spawnPointProjectiles.ForEach(pair => pair.Item2.SetActive(active));
    }
}
