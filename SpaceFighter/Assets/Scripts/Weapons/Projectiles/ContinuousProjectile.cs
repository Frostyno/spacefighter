using MC.Entities;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MC.Weapons
{
    public class ContinuousProjectile : Projectile
    {
        private List<IHealthController> _targets = new List<IHealthController>();

        private void Awake()
        {
            if (TryGetComponent(out Collider2D collider))
                collider.isTrigger = true;
            else
                Debug.LogError($"BulletProjectile on object '{name}' requires Collider2D set as trigger to work properly.");
        }

        private void Update()
        {
            DealDamageToTargets();
        }

        private void DealDamageToTargets()
        {
            var damage = FiredWeaponModel.Damage * Time.deltaTime;
            for (int i = _targets.Count - 1; i >= 0; --i)           // MH: if enemy dies, OnTriggerExit gets called, so no foreach
                _targets[i]?.TakeDamage(damage);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var health = CheckValidTarget(other);
            if (health == null) return;

            _targets.Add(health);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var health = CheckValidTarget(other);
            if (health == null) return;

            _targets.Remove(health);
        }
    }
}
