using UnityEngine;
using MC.Core;
using MC.Entities;

namespace MC.Weapons
{
    public class BulletProjectile : Projectile
    {
        [SerializeField] private float _movementSpeed = 1f;
        
        private void Awake()
        {
            if (TryGetComponent(out Rigidbody2D rigidbody))
                rigidbody.velocity = transform.GetForward() * _movementSpeed;
            else
                Debug.LogError($"BulletProjectile on object '{name}' requires Rigidbody2D to function properly");

            if (TryGetComponent(out Collider2D collider))
                collider.isTrigger = true;
            else
                Debug.LogError($"BulletProjectile on object '{name}' requires Collider2D set as trigger to work properly.");
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var health = CheckValidTarget(other);
            if (health == null) return;

            health.TakeDamage(FiredWeaponModel.Damage);
            Destroy(gameObject);
        }

        private void OnBecameInvisible()
        {
            if (gameObject.activeInHierarchy)
                Destroy(gameObject);
        }
    }
}
