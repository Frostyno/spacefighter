using MC.Core;
using UnityEngine;

namespace MC.Weapons
{
    public class TestingProjectile : ContextMono
    {
        private void Awake()
        {
            GetComponent<Rigidbody2D>().velocity = transform.GetForward() * 9f;
        }

        private void OnBecameInvisible()
        {
            if (gameObject.activeInHierarchy)
                Destroy(gameObject);
        }
    }
}
