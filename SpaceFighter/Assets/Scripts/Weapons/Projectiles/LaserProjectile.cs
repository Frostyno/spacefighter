using System;
using UnityEngine;

namespace MC.Weapons
{
    public class LaserProjectile : Projectile
    {
        private static float _maxRaycastLength = 20f;

        private LineRenderer _lineRenderer = null;

        private void Awake()
        {
            if (TryGetComponent(out _lineRenderer))
                _lineRenderer.SetPositions(new Vector3[] { Vector3.zero, Vector3.zero });
            else
                Debug.LogError($"LaserProjectile on object '{name}' requires LineRenderer to function properly.");
        }

        private void OnDisable()
        {
            for (int i = 0; i < _lineRenderer.positionCount; ++i)
                _lineRenderer.SetPosition(i, Vector3.zero);
        }

        private void Update()
        {
            if (!gameObject.activeInHierarchy) return;

            var hit = Physics2D.Raycast(
                transform.position,
                transform.GetForward(),
                _maxRaycastLength,
                FiredWeaponModel.ValidTargetLayers.value
            );

            if (hit.collider == null)
            {
                UpdatePosition(null);
                return;
            }

            UpdatePosition(hit.point);
            DealDamage(hit.collider);
        }

        private void DealDamage(Collider2D other)
        {
            var health = CheckValidTarget(other);
            if (health == null) return;

            var damage = FiredWeaponModel.Damage * Time.deltaTime;
            health.TakeDamage(damage);
        }

        private void UpdatePosition(Vector2? endPoint)
        {
            _lineRenderer.SetPosition(0, transform.position);
            if (!endPoint.HasValue)
                endPoint = (Vector2)transform.position + (transform.GetForward() * _maxRaycastLength);

            _lineRenderer.SetPosition(1, endPoint.Value);
        }
    }
}
