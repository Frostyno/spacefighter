using MC.Core;
using MC.Entities;
using MC.Models;
using UnityEngine;

namespace MC.Weapons
{
    public class Projectile : ContextMono
    {
        public WeaponModel FiredWeaponModel { get; set; } = null;

        protected IHealthController CheckValidTarget(Collider2D other)
        {
            bool validTarget = FiredWeaponModel.ValidTargetLayers.CheckLayerOverlap(other.gameObject.layer);
            if (!validTarget) return null;
            return other.GetComponent<IHealthController>();
        }
    }
}
