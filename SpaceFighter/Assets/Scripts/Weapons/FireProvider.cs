using WeaponModel = MC.Models.WeaponModel;
using GameContext = MC.Core.GameContext;

namespace MC.Weapons
{
    public abstract class FireProvider
    {
        public GameContext Context { get; private set; }
        public WeaponModel Model { get; private set; }
        public bool Active { get; private set; }

        protected bool Depleted { get; private set; } = false;

        public FireProvider(WeaponModel model, GameContext context)
        {
            Model = model;
            Context = context;
        }

        public virtual void SetActive(bool active) => Active = active;


        /// <summary>
        /// Tries to use requested amount of ammo. If there is not enough ammo in
        /// current magazine, tries to reload.
        /// </summary>
        /// <param name="requestedAmount">Amount of ammo to use.</param>
        /// <returns>Requested ammo amount or less, if not enough ammo is available.</returns>
        protected float UseAmmo(float requestedAmount)
        {
            if (Depleted) return 0f;
            if (!Model.UseAmmo) return float.MaxValue;

            if (Model.CurrentMagazineAmmo >= requestedAmount)
            {
                Model.CurrentMagazineAmmo -= requestedAmount;
                return requestedAmount;
            }

            float oldMagazineLeftover = Model.CurrentMagazineAmmo;
            if (!TryReload())
            {
                Model.CurrentMagazineAmmo = 0f;
                Model.OnWeaponDepleted?.Invoke(Model);
                Depleted = true;

                return oldMagazineLeftover;
            }

            Model.CurrentMagazineAmmo -= (requestedAmount - oldMagazineLeftover);
            return requestedAmount;
        }

        private bool TryReload()
        {
            if (Model.MagazineCount <= 0) return false;

            Model.CurrentMagazineAmmo = Model.MagazineCapacity;
            --Model.MagazineCount;
            return true;
        }

        public abstract void Tick();
    }
}
