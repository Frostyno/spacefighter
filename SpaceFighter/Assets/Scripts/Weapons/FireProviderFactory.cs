using WeaponModel = MC.Models.WeaponModel;
using GameContext = MC.Core.GameContext;
using AttackType = MC.Helpers.Enums.AttackType;

namespace MC.Weapons
{
    public static class FireProviderFactory
    {
        public static FireProvider Create(WeaponModel model, GameContext context)
        {
            return model.AttackType switch
            {
                AttackType.Projectile => new ProjectileFireProvider(model, context),
                AttackType.Continuous => new ContinuousFireProvider(model, context),
                _ => null,
            };
        }
    }
}
