using MC.Core;
using UnityEngine;
using WeaponModel = MC.Models.WeaponModel;

namespace MC.Weapons
{
    public class Weapon : ContextMono
    {
        [SerializeField] private Transform[] _projectileSpawnPoints = new Transform[0];

        private FireProvider _fireProvider = null;

        public WeaponModel Model { get; private set; } = null;

        public void SetModel(WeaponModel model)
        {
            if (Model != null) return;
            Model = model;
            Model.ProjectileSpawnPoints = _projectileSpawnPoints;

            _fireProvider = FireProviderFactory.Create(model, Context);

            if (Model.ProjectilePrefab == null)
                Debug.LogError($"Weapon {name} is missing projectile prefab.");
            if (_projectileSpawnPoints.Length == 0)
                Debug.LogError($"Weapon {name} is missing projectile spawn points.");
        }

        private void Update()
        {
            _fireProvider.Tick();
        }

        public void SetActive(bool active) => _fireProvider.SetActive(active);
    }
}
