using UnityEngine;
using MC.Helpers.Enums;
using GameContext = MC.Core.GameContext;
using WeaponModel = MC.Models.WeaponModel;

namespace MC.Weapons
{
    /// <summary>
    /// Fire provider that spawns projectiles but doesn't retain control
    /// over them. Used for bullet like providers, spawners (mines) etc.
    /// </summary>
    public class ProjectileFireProvider : FireProvider
    {
        private static readonly float c_ammoPerProjectile = 1f;

        public ProjectileFireProvider(WeaponModel model, GameContext context) : base(model, context) { }

        private int _currentSpawnPointIndex = 0;

        private bool CanFire => ((Model.TimeSinceLastFire >= Model.AttackSpeed) && !Depleted);

        public override void Tick()
        {
            UpdateTimers();
            Fire();
        }

        private void UpdateTimers()
        {
            if (!CanFire)
                Model.TimeSinceLastFire += Time.deltaTime;
        }
        public void Fire()
        {
            if (!Active) return;
            if (!CanFire) return;

            switch (Model.ProjectileFireMode)
            {
                case ProjectileFireMode.Simultaneous:
                    SimultaneousFire();
                    break;
                case ProjectileFireMode.Alternating:
                    AlternatingFire();
                    break;
            }

            Model.TimeSinceLastFire = 0f;
        }

        private void SimultaneousFire()
        {
            float ammoToRequest = c_ammoPerProjectile * Model.ProjectileSpawnPoints.Length;
            float availableAmmo = UseAmmo(ammoToRequest);
            foreach (var spawnPoint in Model.ProjectileSpawnPoints)
            {
                if (availableAmmo < c_ammoPerProjectile) break;

                SpawnProjectile(spawnPoint.position, spawnPoint.rotation);
                availableAmmo -= c_ammoPerProjectile; 
            }
        }

        private void AlternatingFire()
        {
            float availableAmmo = UseAmmo(c_ammoPerProjectile); // MH: alternating fire always fires 1 projectile, which uses 1 ammo
            if (availableAmmo < c_ammoPerProjectile) return;

            var spawnPoint = Model.ProjectileSpawnPoints[_currentSpawnPointIndex];
            SpawnProjectile(spawnPoint.position, spawnPoint.rotation);

            ++_currentSpawnPointIndex;
            if (_currentSpawnPointIndex >= Model.ProjectileSpawnPoints.Length)
                _currentSpawnPointIndex = 0;
        }

        private void SpawnProjectile(Vector3 position, Quaternion rotation)
        {
            var projectileGO = GameObject.Instantiate(Model.ProjectilePrefab, position, rotation, Context.ProjectileParent);
            var projectile = projectileGO.GetComponentInChildren<Projectile>(true);
            if (projectile != null)
                projectile.FiredWeaponModel = Model;
        }
    }
}
