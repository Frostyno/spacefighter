namespace MC.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
