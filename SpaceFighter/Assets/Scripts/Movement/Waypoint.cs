using UnityEngine;

namespace MC.Movement
{
    [System.Serializable]
    public class Waypoint
    {
        [SerializeField] private Vector2 _position = Vector2.zero;
        [SerializeField] private float _stoppingDistance = -1f;
        [SerializeField] private float _lingerTime = 0f;

        public Vector2 Position
        {
            get => _position;
            set => _position = value;
        }

        public float? StoppingDistance => (_stoppingDistance < 0f ? null : (float?)_stoppingDistance);
        public float LingerTime => _lingerTime;

        public Waypoint() { }

        public Waypoint(Waypoint other)
        {
            _position = other.Position;
            _stoppingDistance = other.StoppingDistance ?? -1f;
        }
    }
}
