using UnityEngine;
using MC.Core;
using System;
using MC.Models;

namespace MC.Movement
{
    public class EnemyMover : ContextMono, IInjectable<EntityModel>
    {
        [SerializeField] private Rigidbody2D _rigidbody = null;

        private Transform _target = null;
        private Vector2? _targetPosition = null;

        private EnemyModel _model = null;

        public event Action<EnemyModel> OnDestinationReached = null;

        private bool HasTarget => (_target != null || _targetPosition != null);

        public void Inject(EntityModel injectee)
        {
            var enemyModel = injectee as EnemyModel;
            if (enemyModel == null)
            {
                Debug.LogError($"EnemyMover on Enemy {name} requires EnemyModel!");
                return;
            }

            _model = enemyModel;
        }

        public void SetTarget(Transform target)
        {
            _targetPosition = null;
            _target = target;
        }

        public void SetDestination(Vector2 position)
        {
            _target = null;
            _targetPosition = position;
        }

        private void Update()
        {
            if (_model == null) return;
            Move();
        }

        private void Move()
        {
            if (!HasTarget) return;

            var targetPosition = GetTargetPosition();
            Vector2 targetVector = (targetPosition - (Vector2)transform.position);
            float dotProduct = Vector2.Dot(targetVector.normalized, transform.GetForward().normalized);

            if (ReachedTarget(targetVector))
            {
                _rigidbody.velocity = Vector2.zero;
                ResetTarget();
                OnDestinationReached?.Invoke(_model);
                return;
            }

            //_rigidbody.velocity = targetVector.normalized * _model.MovementSpeed;

            // MH: Temporarely disabled until MovementProviders are implemented (Forward, Direct, Blink etc..)
            if (dotProduct < 0.99999f) // MH: account for some imprecision
                RotateTowardsTarget(targetVector);

            if (dotProduct < 0.95f) return;

            //dotProduct = Mathf.Clamp(dotProduct, 0f, 1f);
            _rigidbody.velocity = transform.GetForward() * _model.MovementSpeed;// * dotProduct;
        }

        private Vector2 GetTargetPosition()
        {
            if (_target != null)
                return _target.position;

            return _targetPosition ?? transform.position;
        }

        private void RotateTowardsTarget(Vector2 targetVector)
        {
            float rotationDir = Mathf.Sign(Vector3.Cross(transform.GetForward().normalized, targetVector.normalized).z);
            float newRotation = transform.eulerAngles.z + (_model.AngularSpeed * rotationDir * Time.deltaTime);
            transform.eulerAngles = transform.eulerAngles.With(z: newRotation);
        }

        private bool ReachedTarget(Vector2 targetVector) =>
            targetVector.sqrMagnitude <= _model.SquaredStoppingDistance;

        private void ResetTarget()
        {
            _target = null;
            _targetPosition = null;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, (Vector2)transform.position + transform.GetForward());

            if (!HasTarget || _model == null) return;
            var targetPosition = GetTargetPosition();
            Gizmos.DrawWireSphere(targetPosition, _model.StoppingDistance);
        }
    }
}
