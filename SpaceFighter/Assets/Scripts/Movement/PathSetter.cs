using MC.Core;
using UnityEngine;

namespace MC.Movement
{
    public class PathSetter : ContextMono
    {
        [SerializeField] private EnemyMover _enemyMover = null;
        [SerializeField] private Transform _pathRoot = null;

        private int _currentIndex = 0;

        private void Start()
        {
            if (_enemyMover == null && !TryGetComponent(out _enemyMover))
            {
                Debug.LogError($"PathSetter on object {name} requires EnemyMover component.");
                return;
            }

            if (_pathRoot == null || _pathRoot.childCount == 0)
            {
                Debug.LogError($"PathSetter on object {name} must have path root with at least one child.");
            }

            _enemyMover.OnDestinationReached += HandleDestinationReached;
            _enemyMover.SetTarget(_pathRoot.GetChild(0));
        }

        private void HandleDestinationReached(Models.EnemyModel enemyModel)
        {
            _currentIndex = ++_currentIndex % _pathRoot.childCount;
            _enemyMover.SetTarget(_pathRoot.GetChild(_currentIndex));
        }
    }
}
