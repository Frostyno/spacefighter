using MC.Helpers.Enums;
using UnityEngine;

namespace MC.Movement
{
    [CreateAssetMenu(fileName = "GridSquadronSettings", menuName = "SpaceFighter/Movement/GridSquadronSettings")]   
    public class GridSquadronSettings : SquadronSettings
    {
        public override SquadronType SquadronType => SquadronType.Grid;

        [Header("Grid settings")]
        [SerializeField] private Vector2 _spacing = Vector2.zero;
        [SerializeField] private int _enemiesPerRow = 1;

        public Vector2 Spacing => _spacing;
        public int EnemiesPerRow => Mathf.Clamp(_enemiesPerRow, 1, int.MaxValue);
    }
}
