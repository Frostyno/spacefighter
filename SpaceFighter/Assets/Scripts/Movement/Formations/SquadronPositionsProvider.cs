using MC.Entities;
using MC.Helpers.Enums;
using Vector2 = UnityEngine.Vector2;

namespace MC.Movement
{
    public abstract class SquadronPositionsProvider
    {
        public abstract SquadronType GetSquadronType();

        public abstract void SetWaypointToSquadron(Squadron squadron, Waypoint waypoint);

        protected void SetDestinationToEnemy(EnemyController enemy, Vector2 destination, float? stoppingDistance)
        {
            if (enemy == null) return;

            var enemyMover = enemy.GetComponent<EnemyMover>();
            if (enemyMover == null) return;

            if (stoppingDistance.HasValue)
            {
                var enemyModel = enemy.Model as Models.EnemyModel;
                if (enemyModel != null)
                    enemyModel.SetStoppingDistance(stoppingDistance.Value);
            }

            enemyMover.SetDestination(destination);
        }
    }
}
