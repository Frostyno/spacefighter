using MC.Core;
using MC.Entities;
using MC.Models;
using System.Collections.Generic;
using System;
using UnityEngine;
using IEnumerator = System.Collections.IEnumerator;

namespace MC.Movement
{
    public class Squadron : ContextMono
    {
        [SerializeField] private SquadronSettings _settings = null;

        private List<Tuple<int, EnemyController>> _enemies;
        private List<EnemyController> _movingEnemies;
        private int _currentWaypointIndex = 0;

        private bool HasWaypoints => (_settings != null && _settings.Waypoints.Length > 0);

        public SquadronSettings Settings => _settings;
        public List<Tuple<int, EnemyController>> Enemies => _enemies; 

        private IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();

            int index = 0;
            var enemies = GetComponentsInChildren<EnemyController>();
            _enemies = new List<Tuple<int, EnemyController>>(enemies.Length);
            foreach (var enemy in enemies)
                _enemies.Add(Tuple.Create(index++, enemy));

            _movingEnemies = new List<EnemyController>(_enemies.Count);

            foreach (var enemy in _enemies)
            {
                var model = enemy.Item2.Model;
                model.OnDestroyed.Event += HandleEnemyDestroyed;
                if (enemy.Item2.TryGetComponent<EnemyMover>(out var mover))
                    mover.OnDestinationReached += HandleEnemyDestinationReached;
            }

            StartCoroutine(GoToNextWaypoint(true));
        }

        private void HandleEnemyDestroyed(EntityModel entityModel)
        {
            var enemy = entityModel.Entity as EnemyController;
            if (enemy == null) return;

            var removed = _enemies.Remove(_enemies.Find(e => e.Item2 == enemy));
            if (!removed) return;
            _movingEnemies.Remove(enemy);

            entityModel.OnDestroyed.Event -= HandleEnemyDestroyed;
            if (enemy.TryGetComponent<EnemyMover>(out var mover))
                mover.OnDestinationReached -= HandleEnemyDestinationReached;
            
            if (_enemies.Count == 0)
                Destroy(gameObject);
        }

        private void HandleEnemyDestinationReached(EnemyModel enemyModel)
        {
            var enemy = enemyModel.Entity as EnemyController;
            if (enemy == null) return;

            _movingEnemies.Remove(enemy);
            if (_movingEnemies.Count > 0) return;

            StartCoroutine(GoToNextWaypoint());
        }

        private IEnumerator GoToNextWaypoint(bool firstTime = false)
        {
            if (!HasWaypoints) yield break;
            var pathEndReached = ((!_settings.LoopPath) && (_currentWaypointIndex == (_settings.Waypoints.Length - 1)) && (_currentWaypointIndex != 0 && firstTime));
            if (pathEndReached)
                yield break;

            var waypoint = _settings.Waypoints[_currentWaypointIndex];
            if (waypoint.LingerTime > 0f)
                yield return new WaitForSeconds(waypoint.LingerTime);

            if (!firstTime)
                _currentWaypointIndex = ++_currentWaypointIndex % _settings.Waypoints.Length;
            waypoint = _settings.Waypoints[_currentWaypointIndex];
            if (waypoint == null) 
                yield break;

            foreach (var enemy in _enemies)
                _movingEnemies.Add(enemy.Item2);
            Context.SquadronManager.SetWaypointToSquadron(this, waypoint);
        }

        private void OnDrawGizmosSelected()
        {
            if (_settings == null) return;

            Gizmos.color = Color.cyan;
            foreach (var waypoint in _settings.Waypoints)
                Gizmos.DrawWireSphere(new Vector3(waypoint.Position.x, waypoint.Position.y, 5f), 0.1f);
        }
    }
}
