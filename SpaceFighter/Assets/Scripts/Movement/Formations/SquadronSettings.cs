using MC.Helpers.Enums;
using UnityEngine;

namespace MC.Movement
{
    public abstract class SquadronSettings : ScriptableObject
    {
        [SerializeField] private bool _retainIndex = true;
        [SerializeField] private bool _loopPath = false;
        [SerializeField] private Waypoint[] _waypoints = new Waypoint[0];

        public bool RetainIndex => _retainIndex;
        public bool LoopPath => _loopPath;
        public Waypoint[] Waypoints => _waypoints;
        public abstract SquadronType SquadronType { get; }
    }
}
