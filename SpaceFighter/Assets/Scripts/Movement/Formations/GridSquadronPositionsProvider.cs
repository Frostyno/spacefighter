using MC.Helpers.Enums;

namespace MC.Movement
{
    public class GridSquadronPositionsProvider : SquadronPositionsProvider
    {
        public override SquadronType GetSquadronType() => SquadronType.Grid;

        public override void SetWaypointToSquadron(Squadron squadron, Waypoint waypoint)
        {
            if (squadron.Settings == null) return;
            if (squadron.Settings.SquadronType != GetSquadronType())
                return;

            var settings = squadron.Settings as GridSquadronSettings;
            if (settings == null)
            {
                UnityEngine.Debug.LogError($"Squadron {squadron.name} has incorrect settings type set.");
                return;
            }

            int index = 0;
            foreach (var indexEnemyPair in squadron.Enemies)
            {
                var enemyIndex = settings.RetainIndex ? indexEnemyPair.Item1 : index;
                int row = enemyIndex / settings.EnemiesPerRow;
                int column = enemyIndex % settings.EnemiesPerRow;

                float positionX = (waypoint.Position.x + (column * settings.Spacing.x));
                float positionY = (waypoint.Position.y + (row * settings.Spacing.y));
                var destination = new UnityEngine.Vector2(positionX, positionY);

                SetDestinationToEnemy(indexEnemyPair.Item2, destination, waypoint.StoppingDistance);

                ++index;
            }
        }
    }
}
