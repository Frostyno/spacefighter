using MC.Core;
using MC.Models;
using UnityEngine;

namespace MC.Movement
{
    public class PlayerMover : ContextMono, IInjectable<EntityModel>
    {
        [SerializeField] private Rigidbody2D _rigidbody = null;

        private EntityModel _entityModel = null;

        private void Awake()
        {
            if (_rigidbody == null && !TryGetComponent(out _rigidbody))
                Debug.LogError("PlayerMover is missing Rigidbody2D.");
        }

        public void Inject(EntityModel entityModel) =>
            _entityModel = entityModel;

        public void Move(Vector2 input)
        {
            if (_rigidbody == null)
                return;

            var velocity = new Vector2(input.normalized.x * _entityModel.MovementSpeed, 0f);
            _rigidbody.velocity = velocity;
        }
    }
}
