using UnityEngine;
using MC.InputSystem;
using MC.Movement;
using MC.Definitions;
using WeaponsController = MC.Weapons.WeaponsController;
using WeaponGroup = MC.Helpers.Enums.WeaponGroup;

namespace MC.Entities
{
    public class PlayerController : EntityController
    {
        [SerializeField] private EntityDefinition _definition = null; // TODO: replace for PlayerDefinition later

        [Header("Dependencies")]
        [SerializeField] private PlayerMover _mover = null; // MH: maybe IMover?

        private IPlayerInputController _inputController;

        private void Start()
        {
            if (_mover == null && !TryGetComponent(out _mover))
                Debug.LogError("PlayerController requires PlayerMover to enable movement.");

            Initialize(Context.PlayerModel, _definition);
            _inputController = Context.PlayerInputManager.RequestInputController(InputType.Keyboard);
            _inputController.OnPrimaryFireActiveChanged += HandlePrimaryFireActiveChanged;
            _inputController.OnSecondaryFireActiveChanged += HandleSecondaryFireActiveChanged;
        }

        private void OnDestroy()
        {
            if (_inputController == null) return;

            _inputController.OnPrimaryFireActiveChanged -= HandlePrimaryFireActiveChanged;
            _inputController.OnSecondaryFireActiveChanged -= HandleSecondaryFireActiveChanged;
        }

        private void Update()
        {
            foreach (var pivot in WeaponPivots)
            {
                var targetingAngle = _inputController.CalculateTargetingAngle(pivot.position);
                pivot.eulerAngles = pivot.eulerAngles.With(z: targetingAngle);
            }
        }

        private void FixedUpdate()
        {
            _mover.Move(_inputController.MovementInput);
        }

        private void HandlePrimaryFireActiveChanged(bool active) =>
            WeaponsController.SetWeaponGroupActive(WeaponGroup.Primary, active);
        
        private void HandleSecondaryFireActiveChanged(bool active) =>
            WeaponsController.SetWeaponGroupActive(WeaponGroup.Secondary, active);
    }
}
