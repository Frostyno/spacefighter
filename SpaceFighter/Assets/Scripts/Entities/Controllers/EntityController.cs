using MC.Core;
using MC.Models;
using System.Linq;
using SerializeField = UnityEngine.SerializeField;
using EntityDefinition = MC.Definitions.EntityDefinition;
using WeaponsController = MC.Weapons.WeaponsController;
using UnityEngine;

namespace MC.Entities
{
    public abstract class EntityController : ContextMono
    {
        [SerializeField] private Transform[] _weaponPivots = new Transform[0];

        public EntityModel Model { get; private set; } = null;

        protected WeaponsController WeaponsController { get; private set; }
        protected Transform[] WeaponPivots => _weaponPivots;

        protected void Initialize(EntityModel model, EntityDefinition definition)
        {
            if (definition == null)
            {
                UnityEngine.Debug.LogError($"[EntityController] >> Entity {name} is missing EntityDefinition!");
                return;
            }

            Model = model;
            model.Initialize(this, definition);
            GetComponentsInChildren<IInjectable<EntityModel>>(true).ToList().ForEach(u => u.Inject(model));

            WeaponsController = GetComponent<WeaponsController>();
            if (WeaponsController != null)
                WeaponsController.SpawnWeapons(definition.WeaponDefinitions, definition.ValidTargetLayers);
        }
    }
}
