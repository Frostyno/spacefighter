using System.Collections;
using UnityEngine;
using MC.Definitions;
using MC.Helpers.Enums;
using MC.Models;

namespace MC.Entities
{
    public class EnemyController : EntityController
    {
        [SerializeField] private EnemyDefinition _definition = null;

        protected EnemyModel _enemyModel = null;
        
        private Coroutine _fireCoroutine;
        protected SpriteRenderer Renderer { get; private set; }

        private void Awake() => Renderer = GetComponent<SpriteRenderer>();

        protected virtual void Start()
        {
            Context.EnemyManager.RegisterEnemy(this);
        }

        public EnemyModel Initialize(long ID)
        {
            _enemyModel = new EnemyModel()
            {
                ID = ID
            };

            Initialize(_enemyModel, _definition);
            _enemyModel.OnDestroyed.Event += HandleDestroyed;
            
            return _enemyModel;
        }

        protected void Fire(float delay, WeaponGroup weaponGroup)
        {
            if (_fireCoroutine != null) return;

            _fireCoroutine = StartCoroutine(FireCoroutine(delay, weaponGroup));
        }

        private IEnumerator FireCoroutine(float delay, WeaponGroup weaponGroup)
        {
            if (WeaponsController == null)
            {
                _fireCoroutine = null;
                yield break;
            }    

            yield return new WaitForSeconds(delay);

            WeaponsController.SetWeaponGroupActive(weaponGroup, true);
            Context.SoundManager.PlaySound(transform.position, Sound.Plasma);
            yield return new WaitForSeconds(0.1f);
            WeaponsController.SetWeaponGroupActive(weaponGroup, false);

            _fireCoroutine = null;
        }

        private void HandleDestroyed(EntityModel model)
        {
            var deathAnimation = _enemyModel?.DeathAnimation;
            
            if (deathAnimation == null) return;

            Instantiate(deathAnimation, transform.position, Quaternion.identity);
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
            if (Context == null)
                return;

            if (!TryGetComponent<Collider2D>(out var collider))
                return;
            if (!Context.CameraManager.IsSeenByActiveCamera(collider))
                return;

            var model = Model as EnemyModel;
            Context.CameraManager.ShakeCamera(model.DeathCameraShakeStrength, model.DeathCameraShakeLengthSecs);
            Context.SoundManager.PlaySound(transform.position, Sound.Explosion);
        }
    }
}
