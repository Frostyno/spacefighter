using MC.Helpers.Enums;
using UnityEngine;

namespace MC.Entities
{
    public class BasicEnemyController : EnemyController
    {
        private void Update()
        {
            if (!Renderer.isVisible) return;
            
            var delay = UnityEngine.Random.Range(_enemyModel.MinFireDelay, _enemyModel.MaxFireDelay);
            Fire(delay, WeaponGroup.Primary);

            var playerPosition = Context.PlayerModel.Entity.transform.position;
            foreach (var pivot in WeaponPivots)
            {
                var targetingAngle = ((Vector2)pivot.position).RotationTo(playerPosition);
                pivot.eulerAngles = pivot.eulerAngles.With(z: targetingAngle);
            }
        }
    }
}
