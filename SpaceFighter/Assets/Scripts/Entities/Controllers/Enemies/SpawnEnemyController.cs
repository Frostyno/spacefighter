using MC.Helpers.Enums;
using UnityEngine;

namespace MC.Entities
{
    public class SpawnEnemyController : EnemyController
    {
        private void Update()
        {
            if (!Renderer.isVisible) return;
            
            Fire(_enemyModel.MinFireDelay, WeaponGroup.Primary);
            
            foreach (var pivot in WeaponPivots)
            {
                var pivotPosition = (Vector2) pivot.position;
                var rotation = pivotPosition.RotationTo(pivotPosition + Vector2.down);
                pivot.eulerAngles = pivot.eulerAngles.With(z: rotation);
            }
        }

        private void OnBecameInvisible()
        {
            if (!TryGetComponent(out IHealthController health))
                return;
            
            health.TakeDamage(Model.HealthCurrent + Model.ShieldCurrent);
        }
    }
}
