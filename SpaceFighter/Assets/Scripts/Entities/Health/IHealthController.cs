namespace MC.Entities
{
    public interface IHealthController
    {
        void TakeDamage(float damage);
    }
}
