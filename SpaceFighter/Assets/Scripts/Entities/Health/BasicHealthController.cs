using MC.Core;
using MC.Models;
using Mathf = UnityEngine.Mathf;

namespace MC.Entities
{
    public class BasicHealthController : ContextMono, IHealthController, IInjectable<EntityModel>
    {
        private EntityModel _model = null;

        public void Inject(EntityModel model) =>
            _model = model;

        public void TakeDamage(float damage)
        {
            if (_model == null)
            {
                UnityEngine.Debug.LogError($"Entity '{name}' can't take damage. Missing model.");
                return;
            }

            if (_model.HealthCurrent <= 0f) // MH: entity is already dead
                return;
            if (damage < 0f)
            {
                UnityEngine.Debug.Log("Can't receive negative damage. Use 'Restore' function to heal entity.");
                return;
            }

            damage = TakeShieldDamage(damage);
            if (damage <= 0f)
                return;

            _model.HealthCurrent = Mathf.Clamp(_model.HealthCurrent - damage, 0f, _model.HealthMax);
            _model.OnHealthChanged?.Invoke(_model);
            if (_model.HealthCurrent > 0f)
                return;

            _model.OnDestroyed?.Invoke(_model);
        }

        private float TakeShieldDamage(float damage)
        {
            if (_model.ShieldCurrent <= 0f)
                return damage;

            if (_model.ShieldCurrent >= damage)
            {
                _model.ShieldCurrent -= damage;
                _model.OnShieldChanged?.Invoke(_model);
                return 0f;
            }

            float overflow = damage - _model.ShieldCurrent;
            _model.ShieldCurrent = 0f;
            _model.OnShieldChanged?.Invoke(_model);
            return overflow;
        }
    }
}
