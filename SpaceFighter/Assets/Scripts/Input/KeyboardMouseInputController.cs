using System;
using UnityEngine;

namespace MC.InputSystem
{
    public class KeyboardMouseInputController : IPlayerInputController
    {
        private Camera _camera = Camera.main;

        private bool _primaryFire = false;
        private bool _secondaryFire = false;

        public Vector2 MovementInput { get; private set; } = Vector2.zero;

        public bool PrimaryFire => _primaryFire;
        public bool SecondaryFire => _secondaryFire;

        public event Action<bool> OnPrimaryFireActiveChanged = null;
        public event Action<bool> OnSecondaryFireActiveChanged = null;

        public float CalculateTargetingAngle(Vector2 pivotPosition)
        {
            Vector2 mousePosition   = _camera.ScreenToWorldPoint(Input.mousePosition);
            var targetingAngle      = pivotPosition.RotationTo(mousePosition);
            return Mathf.Clamp(targetingAngle, -90f, 90f);
        }

        public void Tick()
        {
            float horizontalInput   = Input.GetAxisRaw(InputConstants.HorizontalAxis);
            float verticalInput     = Input.GetAxisRaw(InputConstants.VerticalAxis);
            MovementInput           = new Vector2(horizontalInput, verticalInput);

            CheckFire(ref _primaryFire, OnPrimaryFireActiveChanged, InputConstants.PrimaryFire);
            CheckFire(ref _secondaryFire, OnSecondaryFireActiveChanged, InputConstants.SecondaryFire);
        }

        private void CheckFire(ref bool fire, Action<bool> fireEvent, string button)
        {
            if (Input.GetButtonDown(button))
            {
                fire = true;
                fireEvent?.Invoke(true);
            }
            else if (Input.GetButtonUp(button))
            {
                fire = false;
                fireEvent?.Invoke(false);
            }
        }
    }
}
