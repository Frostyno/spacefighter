namespace MC.InputSystem
{
    public static class InputConstants
    {
        public static readonly string HorizontalAxis = "Horizontal";
        public static readonly string VerticalAxis = "Vertical";
        public static readonly string PrimaryFire = "PrimaryFire";
        public static readonly string SecondaryFire = "SecondaryFire";
    }
}
