using System;
using Vector2 = UnityEngine.Vector2;

namespace MC.InputSystem
{
    public interface IPlayerInputController
    {
        Vector2 MovementInput { get; }
        bool PrimaryFire { get; }
        bool SecondaryFire { get; }


        event Action<bool> OnPrimaryFireActiveChanged;
        event Action<bool> OnSecondaryFireActiveChanged;

        float CalculateTargetingAngle(Vector2 pivotPosition);
        void Tick();
    }
}