namespace MC.InputSystem
{
    public static class InputControllerFactory
    {
        public static IPlayerInputController Create(InputType inputType)
        {
            switch (inputType)
            {
                case InputType.Keyboard:
                    return new KeyboardMouseInputController();
                default:
                    UnityEngine.Debug.LogError($"Invalid InputType '{inputType}.'");
                    return null;
            }
        }
    }
}
