using UnityEngine;

namespace MC.Definitions
{
    [CreateAssetMenu(fileName = "EntityDefinition", menuName = "SpaceFighter/Definitions/Entity")]
    public class EntityDefinition : ScriptableObject
    {
        [Header("Stats settings")]
        [SerializeField] private float _healthMax;
        [SerializeField] private float _shieldMax;

        [Header("Movement settings")]
        [SerializeField, Range(0.5f, 50f)] private float _baseMovementSpeed = 1f;

        [Header("Weapon settings")]
        [SerializeField] private LayerMask _validTargetLayers = 0;
        [SerializeField] private WeaponDefinition[] _weaponDefinitions = new WeaponDefinition[0];

        public float HealthMax => _healthMax;
        public float ShieldMax => _shieldMax;

        public float BaseMovementSpeed => _baseMovementSpeed;

        public LayerMask ValidTargetLayers => _validTargetLayers;
        public WeaponDefinition[] WeaponDefinitions => _weaponDefinitions;
    }
}
