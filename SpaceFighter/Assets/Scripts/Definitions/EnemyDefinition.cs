using UnityEngine;

namespace MC.Definitions
{
    [CreateAssetMenu(fileName = "EnemyDefinition", menuName = "SpaceFighter/Definitions/Enemy")]
    public class EnemyDefinition : EntityDefinition
    {
        [Header("Enemy settings")]
        [SerializeField, Range(0.5f, 1000f)] private float _baseAngularSpeed = 1f;
        [SerializeField, Range(0f, 15f)] private float _defaultStoppingDistance = 1f;
        
        [Space]

        [SerializeField] private float _minFireDelay = 2f;
        [SerializeField] private float _maxFireDelay = 5f;

        [Space]
        
        [Header("DeathSettings")]
        [SerializeField, Range(0f, 5f)] private float _deathCameraShakeStrength = 0.3f;
        [SerializeField, Range(0f, 5f)] private float _deathCameraShakeLengthSecs = 0.3f;
        [SerializeField] private GameObject _deathAnimation = null;


        public float BaseAngularSpeed => _baseAngularSpeed;
        public float DefaultStoppingDistance => _defaultStoppingDistance;
        public float MinFireDelay => Mathf.Clamp(_minFireDelay, 0f, float.MaxValue);
        public float MaxFireDelay => Mathf.Clamp(_maxFireDelay, 0f, float.MaxValue);
        public float DeathCameraShakeStrength => _deathCameraShakeStrength;
        public float DeathCameraShakeLengthSecs => _deathCameraShakeLengthSecs;
        public GameObject DeathAnimation => _deathAnimation;
    }
}
