using UnityEngine;
using MC.Helpers.Enums;
using Weapon = MC.Weapons.Weapon;

namespace MC.Definitions
{
    [CreateAssetMenu(fileName = "WeaponDefinition", menuName = "SpaceFighter/Definitions/Weapon")]
    public class WeaponDefinition : ScriptableObject
    {
        [Header("General settings")]
        [SerializeField] private Weapon _weaponPrefab = null;
        [SerializeField] private WeaponGroup _weaponGroup = WeaponGroup.Primary;

        [Space]

        [SerializeField] private GameObject _projectilePrefab = null;
        [SerializeField] private AttackType _attackType = AttackType.Projectile;

        [SerializeField] private float _attackSpeed = 1f;
        [SerializeField] private float _damage = 1f;
        [SerializeField] private ProjectileFireMode _projectileSpawnType = ProjectileFireMode.Simultaneous;

        [Header("Ammo settings")]
        [SerializeField] private bool _useAmmo = false;
        [SerializeField] private float _magazineCapacity = 10f;
        [SerializeField] private int _spareMagazineCount = 1;

        public Weapon WeaponPrefab => _weaponPrefab;
        public WeaponGroup WeaponGroup => _weaponGroup;

        public float AttackSpeed => _attackSpeed;
        public float Damage => _damage;
        public AttackType AttackType => _attackType;
        public ProjectileFireMode ProjectileFireMode => _projectileSpawnType;
        public GameObject ProjectilePrefab => _projectilePrefab;

        public bool UseAmmo => _useAmmo;
        public float MagazineCapacity => Mathf.Clamp(_magazineCapacity, 0f, float.MaxValue);
        public int SpareMagazineCount => Mathf.Clamp(_spareMagazineCount, 0, int.MaxValue);
    }
}
