namespace MC.AI
{
    public interface ITransition
    {
        IState StateFrom { get; }
        IState StateTo { get; }
        bool Evaluate();
    }
}
