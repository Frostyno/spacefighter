using System.Collections.Generic;

namespace MC.AI
{
    public class StateMachine
    {
        private readonly Dictionary<IState, List<ITransition>> _transitions = new Dictionary<IState, List<ITransition>>();
        private readonly List<ITransition> _anyStateTransitions = new List<ITransition>();

        private IState _currentState = null;

        public void AddTransition(ITransition transition)
        {
            if (transition == null) return;

            if (!_transitions.TryGetValue(transition.StateFrom, out var stateTransitions))
            {
                stateTransitions = new List<ITransition>();
                _transitions.Add(transition.StateFrom, stateTransitions);
            }

            if (!stateTransitions.Contains(transition))
                stateTransitions.Add(transition);
        }

        public void RemoveTransition(ITransition transition)
        {
            if (transition == null) return;

            if (!_transitions.TryGetValue(transition.StateFrom, out var stateTransitions))
                return;

            stateTransitions.Remove(transition);
        }

        public void AddAnyStateTransition(ITransition transition)
        {
            if (transition == null) return;
            if (!_anyStateTransitions.Contains(transition))
                _anyStateTransitions.Add(transition);
        }

        public void RemoveAnyStateTransition(ITransition transition)
        {
            if (transition == null) return;
            _anyStateTransitions.Remove(transition);
        }

        public bool SetState(IState state)
        {
            if (state == null)
            {
                UnityEngine.Debug.LogError("Can't set state to null!");
                return false;
            }

            if (!_transitions.ContainsKey(state))
                UnityEngine.Debug.LogWarning($"State of type {state.GetType()} has no transitions!");

            _currentState?.ExitState();
            _currentState = state;
            _currentState.EnterState();
            return true;
        }

        public void Tick()
        {
            CheckTransitions();
            _currentState?.Tick();
        }

        private void CheckTransitions()
        {
            var transitioned = CheckTransitions(_anyStateTransitions);
            if (transitioned) return;

            if (_currentState == null) return;
            if (!_transitions.TryGetValue(_currentState, out var stateTransitions))
                return;

            CheckTransitions(stateTransitions);
        }

        private bool CheckTransitions(List<ITransition> transitions)
        {
            foreach (var transition in transitions)
            {
                if (!transition.Evaluate()) continue;

                var stateSet = SetState(transition.StateTo);
                if (stateSet)
                    return true;
            }

            return false;
        }
    }
}
