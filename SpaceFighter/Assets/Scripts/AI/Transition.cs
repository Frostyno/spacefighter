using System;
using System.Linq;
using System.Collections.Generic;

namespace MC.AI
{
    public class Transition : ITransition
    {
        private readonly List<Func<bool>> _conditions = new List<Func<bool>>();

        public IState StateTo { get; private set; } = null;
        public IState StateFrom { get; private set; } = null;

        public Transition(IState stateFrom, IState stateTo, Func<bool> condition)
        {
            StateFrom = stateFrom;
            StateTo = stateTo;

            AddCondition(condition);
        }

        public Transition(IState stateFrom, IState stateTo, List<Func<bool>> conditions = null)
        {
            StateFrom = stateFrom;
            StateTo = stateTo;

            AddConditions(conditions);
        }

        public void AddCondition(Func<bool> condition)
        {
            if (condition == null) return;
            _conditions.Add(condition);
        }

        public void AddConditions(List<Func<bool>> conditions)
        {
            if (conditions == null) return;
            var checkedConditions = conditions.Where(c => c != null);
            _conditions.AddRange(checkedConditions);
        }

        public void RemoveCondition(Func<bool> condition) =>
            _conditions.Remove(condition);

        public void RemoveConditions(List<Func<bool>> conditions) =>
            conditions?.ForEach(c => _conditions.Remove(c));

        public bool Evaluate()
        {
            if (_conditions.Count == 0) return false;

            foreach (var condition in _conditions)
                if (!condition())
                    return false;

            return true;
        }
    }
}
