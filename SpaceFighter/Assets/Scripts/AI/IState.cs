namespace MC.AI
{
    public interface IState
    {
        void EnterState();
        void Tick();
        void ExitState();
    }
}
