using MC.Core;
using System.Collections.Generic;
using System.Linq;

namespace MC.UI
{
    public class CanvasRoot : ContextMono
    {
        private void OnEnable()
        {
            if (Context == null)
            {
                StartCoroutine(RegisterCanvas());
                return;
            }

            Context.ScreenManager.RegisterCanvas(this);
        }

        private void OnDisable()
        {
            if (Context == null) return;

            Context.ScreenManager.UnregisterCanvas(this);
        }

        private System.Collections.IEnumerator RegisterCanvas()
        {
            while (Context == null)
                yield return null;

            Context.ScreenManager.RegisterCanvas(this);
        }

        public List<ScreenBase> GetScreens() =>
            GetComponentsInChildren<ScreenBase>(true).ToList();
    }
}
