using MC.Models;
using IEnumerator = System.Collections.IEnumerator;
using SerializeField = UnityEngine.SerializeField;
using TMP_Text = TMPro.TMP_Text;

namespace MC.UI
{
    public class PlayerHealthScreen : ScreenBase
    {
        [SerializeField] private TMP_Text _healthText = null;
        [SerializeField] private TMP_Text _shieldText = null;

        private IEnumerator Start()
        {
            yield return new UnityEngine.WaitForEndOfFrame();

            var playerModel = Context.PlayerModel;

            playerModel.OnHealthChanged.Event += HandleHealthChanged;
            playerModel.OnShieldChanged.Event += HandleShieldChanged;

            HandleHealthChanged(playerModel);
            HandleShieldChanged(playerModel);
        }

        private void OnDestroy()
        {
            var playerModel = Context.PlayerModel;
            if (playerModel == null) return;

            playerModel.OnHealthChanged.Event -= HandleHealthChanged;
            playerModel.OnShieldChanged.Event -= HandleShieldChanged;
        }

        private void HandleShieldChanged(EntityModel model)
        {
            if (model == null) return;

            _shieldText.SetText($"{model.ShieldCurrent}/{model.ShieldMax}");
        }

        private void HandleHealthChanged(EntityModel model)
        {
            if (model == null) return;

            _healthText.SetText($"{model.HealthCurrent}/{model.HealthMax}");
        }
    }
}
