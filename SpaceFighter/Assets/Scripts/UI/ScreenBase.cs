using MC.Core;

namespace MC.UI
{
    public class ScreenBase : ContextMono
    {
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}