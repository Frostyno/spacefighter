using System.Collections.Generic;
using MC.Definitions;
using WeaponGroup = MC.Helpers.Enums.WeaponGroup;
using Weapon = MC.Weapons.Weapon;
using EntityController = MC.Entities.EntityController;

namespace MC.Models
{
    public class EntityModel
    {
        public EntityController Entity { get; private set; } = null;

        /* Stats */
        public float HealthCurrent = 0f;
        public float HealthMax = 0f;
        public float HealthRegeneration = 0f;

        public float ShieldCurrent = 0f;
        public float ShieldMax = 0f;
        public float ShieldRegeneration = 0f;

        public ModelEvent<EntityModel> OnHealthChanged { get; private set; } = new ModelEvent<EntityModel>();
        public ModelEvent<EntityModel> OnDestroyed { get; private set; } = new ModelEvent<EntityModel>();
        public ModelEvent<EntityModel> OnShieldChanged { get; private set; } = new ModelEvent<EntityModel>();

        public float BaseMovementSpeed { get; private set; } = 1f;
        public float MovementSpeed => BaseMovementSpeed; // MH: later this will take other things into account

        /* Weapons */
        public Dictionary<WeaponGroup, List<Weapon>> Weapons { get; private set; } = new Dictionary<WeaponGroup, List<Weapon>>();

        public virtual void Initialize(EntityController entity, EntityDefinition definition)
        {
            Entity = entity;

            if (definition == null)
            {
                UnityEngine.Debug.LogError("Can't initialize entity model without definition.");
                return;
            }

            HealthMax = definition.HealthMax;
            HealthCurrent = HealthMax;

            ShieldMax = definition.ShieldMax;
            ShieldCurrent = ShieldMax;

            BaseMovementSpeed = definition.BaseMovementSpeed;
        }

        // TODO: new constructor which takes save data
    }
}
