using MC.Definitions;
using MC.Entities;
using UnityEngine;

namespace MC.Models
{
    public class EnemyModel : EntityModel
    {
        public long? ID { get; set; } = null;

        public float BaseAngularSpeed { get; private set; } = 1f;
        public float AngularSpeed => BaseAngularSpeed;

        public float DefaultStoppingDistance { get; private set; } = 1f;
        public float StoppingDistance { get; private set; } = 1f;
        public float SquaredStoppingDistance { get; private set; } = 1f;

        public float DeathCameraShakeStrength { get; private set; } = 0.3f;
        public float DeathCameraShakeLengthSecs { get; private set; } = 0.3f;
        public GameObject DeathAnimation { get; private set; } = null;

        public float MinFireDelay { get; private set; } = 2f;
        public float MaxFireDelay { get; private set; } = 5f;

        public override void Initialize(EntityController entity, EntityDefinition definition)
        {
            base.Initialize(entity, definition);

            var enemyDefinition = definition as EnemyDefinition;
            if (enemyDefinition == null)
            {
                UnityEngine.Debug.LogError($"Passed definition for enemy {entity.name} is not an EnemyDefinition!");
                return;
            }

            BaseAngularSpeed = enemyDefinition.BaseAngularSpeed;
            DefaultStoppingDistance = enemyDefinition.DefaultStoppingDistance;
            SetStoppingDistance(DefaultStoppingDistance);
            MinFireDelay = enemyDefinition.MinFireDelay;
            MaxFireDelay = enemyDefinition.MaxFireDelay;

            DeathCameraShakeStrength = enemyDefinition.DeathCameraShakeStrength;
            DeathCameraShakeLengthSecs = enemyDefinition.DeathCameraShakeLengthSecs;
            DeathAnimation = enemyDefinition.DeathAnimation;
        }

        public void SetStoppingDistance(float stoppingDistance)
        {
            StoppingDistance = stoppingDistance;
            SquaredStoppingDistance = UnityEngine.Mathf.Pow(stoppingDistance, 2f);
        }
    }
}
