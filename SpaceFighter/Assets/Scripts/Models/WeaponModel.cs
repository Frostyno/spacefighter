using MC.Helpers.Enums;
using UnityEngine;
using Weapon = MC.Weapons.Weapon;
using WeaponDefinition = MC.Definitions.WeaponDefinition;
using GameObject = UnityEngine.GameObject;
using Transform = UnityEngine.Transform;

namespace MC.Models
{
    public class WeaponModel
    {
        public WeaponGroup WeaponGroup { get; private set; } = WeaponGroup.Primary;

        public float AttackSpeed { get; private set; } = 1f;
        public float Damage { get; private set; } = 1f;
        public AttackType AttackType { get; private set; } = AttackType.Projectile;
        public ProjectileFireMode ProjectileFireMode { get; private set; } = ProjectileFireMode.Simultaneous;
        public GameObject ProjectilePrefab { get; private set; } = null;

        public bool UseAmmo { get; private set; } = false;
        public float MagazineCapacity { get; private set; } = 0f;

        public LayerMask ValidTargetLayers { get; private set; } = 0;
        public Weapon Weapon { get; private set; } = null;
        public float TimeSinceLastFire { get; set; } = float.MaxValue;
        public int MagazineCount { get; set; } = 0;
        public float CurrentMagazineAmmo { get; set; } = 0f;
        public Transform[] ProjectileSpawnPoints { get; set; } = new Transform[0];

        public ModelEvent<WeaponModel> OnWeaponDepleted { get; private set; } = new ModelEvent<WeaponModel>();

        public WeaponModel(WeaponDefinition definition, Weapon weapon, LayerMask validTargetLayers)
        {
            Weapon = weapon;
            ValidTargetLayers = validTargetLayers;
            WeaponGroup = definition.WeaponGroup;

            AttackSpeed = definition.AttackSpeed;
            Damage = definition.Damage;
            AttackType = definition.AttackType;
            ProjectileFireMode = definition.ProjectileFireMode;
            ProjectilePrefab = definition.ProjectilePrefab;
            if (definition.ProjectilePrefab == null)
                Debug.LogError($"Weapon {definition.name} is missing projectile prefab.");

            if (!definition.UseAmmo) return;

            UseAmmo = definition.UseAmmo;
            MagazineCapacity = definition.MagazineCapacity;
            CurrentMagazineAmmo = definition.MagazineCapacity;
            MagazineCount = definition.SpareMagazineCount;
        }
    }
}
