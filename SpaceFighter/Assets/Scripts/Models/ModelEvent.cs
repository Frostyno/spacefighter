using System;

namespace MC.Models
{
    public class ModelEvent<ModelType>
    {
        public event Action<ModelType> Event = null;

        public void Invoke(ModelType model) => // MH: maybe add lock object to restrict who can trigger this event
            Event?.Invoke(model);
    }
}
