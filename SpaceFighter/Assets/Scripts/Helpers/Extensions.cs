using UnityEngine;

namespace MC
{
    public static class Extensions
    {
        public static Vector2 GetForward(this Transform transform)
        {
            float rotation = (transform.eulerAngles.z + 90f) * Mathf.Deg2Rad;
            return new Vector2(Mathf.Cos(rotation), Mathf.Sin(rotation));
        }

        public static float RotationTo(this Vector2 from, Vector2 to, Vector2? zeroRotationVector = null)
        {
            var zeroRotation    = zeroRotationVector ?? Vector2.up;

            var vectorFromTo    = to - from;
            var angle           = Vector2.Angle(zeroRotation.normalized, vectorFromTo.normalized);

            return angle * Mathf.Sign(-vectorFromTo.x);
        }

        public static Vector3 With(this Vector3 vector, float? x = null, float? y = null, float? z = null)
        {
            if (x.HasValue) vector.x = x.Value;
            if (y.HasValue) vector.y = y.Value;
            if (z.HasValue) vector.z = z.Value;
            return vector;
        }

        public static bool CheckLayerOverlap(this LayerMask mask, int layer)
        {
            return (mask == (mask | (1 << layer)));
        }
    }
}
