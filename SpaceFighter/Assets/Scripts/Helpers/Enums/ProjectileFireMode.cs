﻿namespace MC.Helpers.Enums
{
    public enum ProjectileFireMode
    {
        Simultaneous = 0,
        Alternating = 1
    }
}
