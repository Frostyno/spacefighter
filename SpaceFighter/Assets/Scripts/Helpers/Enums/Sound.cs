namespace MC.Helpers.Enums
{
    public enum Sound
    {
        None,
        Plasma,
        Explosion,
        Laser,
        Background,
    }
}
