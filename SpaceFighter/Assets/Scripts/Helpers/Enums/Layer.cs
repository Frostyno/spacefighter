namespace MC.Helpers.Enums
{
    public enum Layer
    {
        Environment = 6,
        Player = 7,
        Enemies = 8,
    }
}
