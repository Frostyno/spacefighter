namespace MC.Helpers.Enums
{
    public enum AttackType
    {
        Projectile,
        Continuous
    }
}
