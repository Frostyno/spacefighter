using System;
using MC.Core;
using UnityEngine;

namespace MC.Helpers
{
    public class Disposer : ContextMono
    {
        [SerializeField] private float _disposeTime = 1f;

        private System.Collections.IEnumerator Start()
        {
            if (_disposeTime > 0f)
                yield return new WaitForSeconds(_disposeTime);
            
            Destroy(gameObject);
        }
    }
}
